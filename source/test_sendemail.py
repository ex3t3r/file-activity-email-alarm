# test_sendemail
from datetime import datetime
import sendemail
import json
import sys
import pathlib


class JSONData(object):
    def __init__(self, json_data):
        self.__dict__ = json_data


def test_send_email(sender_email, receiver_email_list,
                    password, smtp_server, smtp_port, use_tls, email_body):
    """ Test the email sending functionality.

    :param sender_email: String, email address of the sender
    :param receiver_email_list: List of Strings, email addresses of receivers
                                of the email.
    :param password: String, SMTP password of sender account
    :param smtp_server: String, name of SMTP server to use
    :param smtp_port: Int, the port to use on the SMTP server, normally 587
    :param use_tls: bool, true if TLS method should be used with SMTP server
    :param email_body: JSON objcet, containing the content of the email body
    """
    # subject = 'Test email ' + datetime.now().strftime('%Y/%m/%d')
    subject = email_body["subject"] + datetime.now().strftime('%Y/%m/%d')

    # create the email instance
    test_email = sendemail.SendEmail(sender_email, receiver_email_list, subject,
                                     password, smtp_server, smtp_port, use_tls)

    for item in email_body["body"]:
        if "html" in item.keys():
            test_email.html_add(item["html"])
        elif "html_block" in item.keys():
            # Support the inclusion of multi-line string blocks of HTML/text
            test_email.html_add("\n".join(item["html_block"]))
        elif "image" in item.keys():
            test_email.add_inline_image(item["image"])
        elif "attach" in item:
            test_email.add_attach(item["attach"])

    # And finally send the email
    test_email.send()


# example below
if __name__ == '__main__':
    # Read command line parameter for the test config path and filename.
    path = sys.argv[1] if len(sys.argv) > 1 else './config.json'
    file = pathlib.Path(path)
    # Read the SMTP configuration
    if file.exists() & file.is_file():
        with open(path) as f:
            data = json.load(f)
        config_data = JSONData(data)
    else:
        print("Specify the correct path and filename to the config file.\n\
              Alternatively, the default filename and path \'./config.json\' will be used.")
        exit(0)

    try:
        smtp_server = config_data.configuration["smtp"]["smtp_server"]
        smtp_port = config_data.configuration["smtp"]["smtp_port"]
        use_tls = config_data.configuration["smtp"]["smtp_over_tls"]
        email_sender = config_data.configuration["email"]["email_sender"]
        email_receiver_list = config_data.configuration["email"]["email_receiver"]
        smtp_password = config_data.configuration["smtp"]["smtp_password"]
        email_body = config_data.email_bodies_for_events["test"]
    except Exception as e:
        print("I got a Key Error - reason: item {} is not defined.\n\
              Correct before continueing".format(str(e)))
        exit(0)

    # Send a test email
    test_send_email(email_sender, email_receiver_list,
                    smtp_password, smtp_server, smtp_port, use_tls, email_body)
