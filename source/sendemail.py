#
from os.path import basename

import smtplib
import ssl
import mimetypes
from email.mime.multipart import MIMEMultipart
from email import encoders
from email.mime.audio import MIMEAudio
from email.mime.base import MIMEBase
from email.mime.image import MIMEImage
from email.mime.text import MIMEText


class SendEmail:
    """ Sending HTML encoded email supporting inline images and attachments

    :param sender: String, email address of the sender.
    :param recipients: List of strings, email addresses of the recipients.
                       For example ['recipient1@gmail.com']
    :param subject: String, the email subject line.
    :param password: String, the SMTP server login password of sender.
    :param smtp_server: String, the name of the SMTP server to use.
    :param smtp_port: Int, the port number to use.
    :param use_tls: Bool, Force the usage of TLS with SMTP
    """

    def __init__(self, sender, recipients, subject,
                 password, smtp_server, smtp_port, use_tls):
        self.subject = subject
        self.recipients = recipients
        self.html_body = ''
        self.sender = sender
        self.sender_password = password
        self.attachments = []
        self.smtp_server = smtp_server
        self.port = smtp_port
        self.use_tls = use_tls

    def send(self):
        """ Construct the email body and send it via SMTP

        """
        msg = MIMEMultipart('alternative')
        # Build the email header
        msg['From'] = self.sender
        msg['Subject'] = self.subject
        msg['To'] = ", ".join(self.recipients)
        msg.preamble = "preamble goes here"

        # Check if there are attachments if yes, add them
        if self.attachments:
            self.attach(msg)

        # Add html body after attachments
        msg.attach(MIMEText(self.html_body, 'html'))

        # send the email
        if self.use_tls:
            # Log in to server using secure context and send email
            context = ssl.create_default_context()
            with smtplib.SMTP(self.smtp_server, self.port) as server:
                server.starttls(context=context)
                server.login(self.sender, self.sender_password)
                server.sendmail(self.sender, self.recipients, msg.as_string())
        else:
            # Use SSL from the start of connection.
            with smtplib.SMTP_SSL(host=self.smtp_server, port=self.port) as server:
                server.login(self.sender, self.sender_password)
                server.sendmail(self.sender, self.recipients, msg.as_string())

    def html_add(self, html):
        """ Add a HTML encoded section to the email body.

        :param html: String, the html content to add.
        """
        self.html_body = self.html_body + '<p></p>' + html

    def attach(self, msg):
        """Attach the various inline content and the attachments to the email msg.

        :param msg: MIMEMultipart, message build up based on message items.
        """
        for f in self.attachments:

            ctype, encoding = mimetypes.guess_type(f)
            if ctype is None or encoding is not None:
                ctype = "application/octet-stream"

            maintype, subtype = ctype.split("/", 1)

            if maintype == "text":
                fp = open(f)
                # Note: we should handle calculating the charset
                attachment = MIMEText(fp.read(), _subtype=subtype)
                fp.close()
            elif maintype == "image":
                fp = open(f, "rb")
                attachment = MIMEImage(fp.read(), _subtype=subtype, name=basename(f))
                fp.close()
            elif maintype == "audio":
                fp = open(f, "rb")
                attachment = MIMEAudio(fp.read(), _subtype=subtype, name=basename(f))
                fp.close()
            else:
                fp = open(f, "rb")
                attachment = MIMEBase(maintype, subtype)
                attachment.set_payload(fp.read())
                fp.close()
                encoders.encode_base64(attachment)
            attachment.add_header("Content-Disposition", "attachment",
                                      filename=basename(f))
            attachment.add_header('Content-ID', '<{}>'.format(basename(f)))
            msg.attach(attachment)

    def add_attach(self, files):
        """ Attach the list of files to the email.

        :param files: list of strings, the filenames of the files to attach.
        """
        self.attachments = self.attachments + files

    def add_inline_image(self, image_file):
        """ Attach an inline image into the HTML emal body

        :param image_file: String, filemane of the image to insert
        """
        # attach image chart
        file_list = [image_file]
        self.add_attach(file_list)

        # Create the Content ID to use
        image_cid = '<img src="cid:{}"/>'.format(basename(image_file))
        # refer to image chart in html
        self.html_add(image_cid)
