"""
Building a Python filesystem watchdog

Commandline Parameters: (All optional)
   -m --monitor: full path to monitor. Default: current folder.
   -c --config: config file Path and filename. Default:  ./config.json
   -p --pattern: Monitoring pattern. Default '*'
   -a, --ignore_pattern: The ignore pattern. Default: ''
   -i, --ignore_directories: Directories to be monitored: Default: True
   -s, --case_sensitive: Case sensitive monitoring. Default: True
   -r, --recursive: Recursive directories Monitoring. Default: True

"""
import argparse
import time
import pathlib
import json
import sendemail
from datetime import datetime
from watchdog.observers import Observer
from watchdog.events import PatternMatchingEventHandler


class JSONData(object):
    """ Insert the json_data string into a JSON object
    """
    def __init__(self, json_data):
        self.__dict__ = json_data


class FileChangeMonitor:
    """ Monitor for any changes in a given path and execute an action
        based on event

    :params monitor_path: String, path to be monitored
    :params monitor_recursive: Bool, should directories be recursively checked
    :param patterns: String, pattern of files to monitor
    :param ignore_patterns: String, files/ direcotires to be ignored
    :param ignore_directories: Bool, Set to True if directories must be ignored
    :param case_sensitive: Bool, True to use case sensitive folder system
    """
    def __init__(self, monitor_path, config_path, monitor_recursive, patterns,
                 ignore_patterns, ignore_directories, case_sensitive):
        self.__monitor_path = monitor_path
        self.__monitor_recursive = monitor_recursive
        self.__watchdog_event_handler = FileActionEventHandler(patterns,
                                                               ignore_patterns,
                                                               ignore_directories,
                                                               case_sensitive,
                                                               config_path)
        self.__watchdog_observer = Observer()

    def run(self):
        """
        """
        self.start()
        try:
            while True:
                time.sleep(1)
        except KeyboardInterrupt:
            self.stop()

    def start(self):
        self.__schedule()
        self.__watchdog_observer.start()

    def stop(self):
        self.__watchdog_observer.stop()
        self.__watchdog_observer.join()

    def __schedule(self):
        self.__watchdog_observer.schedule(self.__watchdog_event_handler,
                                          self.__monitor_path,
                                          recursive=self.__monitor_recursive)


class FileActionEventHandler(PatternMatchingEventHandler):
    """ The event handler. The action methods for each of the possible events.

    :param patterns: String, pattern of files to monitor
    :param ignore_patterns: String, files/ direcotires to be ignored
    :param ignore_directories: Bool, Set to True if directories must be ignored
    :param case_sensitive: Bool, True to use case sensitive folder system
    """
    def __init__(self, monitor_pattern, ignore_patterns, ignore_directories,
                 case_sensitive, config_path):
        super().__init__(monitor_pattern, ignore_patterns,
                         ignore_directories, case_sensitive)
        file = pathlib.Path(config_path)
        # Read the SMTP and HTML body configuration
        if file.exists() & file.is_file():
            with open(config_path) as f:
                all_data = json.load(f)
            self.config_data = JSONData(all_data)
        else:
            print("Please specify the correct path and filename to the \
                  config file to use.\n\
                  Alternatively the default filename and path\
                   \'./config.json\' will be used.")
            exit(0)

        try:
            self.smtp_server = self.config_data.configuration["smtp"]["smtp_server"]
            self.smtp_port = self.config_data.configuration["smtp"]["smtp_port"]
            self.use_tls = self.config_data.configuration["smtp"]["smtp_over_tls"]
            self.__smtp_password = self.config_data.configuration["smtp"]["smtp_password"]
            self.email_sender = self.config_data.configuration["email"]["email_sender"]
            self.email_receiver_list = self.config_data.configuration["email"]["email_receiver"]
        except Exception as e:
            print("I got a Key Error - reason: item {} is not defined.\n\
                  Please correct before continueing".format(str(e)))
            exit(0)

    def on_created(self, event):
        # This method must be updated with the required actions.
        # This is an example of how it could be used.
        print("Create event, {0} was created.".format(event.src_path))
        self.send_email_alarm("created", event.src_path)

    def on_deleted(self, event):
        # This method must be updated with the required actions
        print("Delete event, {0} is deleted".format(event.src_path))
        self.send_email_alarm("deleted", event.src_path)

    def on_modified(self, event):
        # This method must be updated with the required actions
        print("Modified event, {0} was modified".format(event.src_path))
        self.send_email_alarm("modified", event.src_path)

    def on_moved(self, event):
        # This method must be updated with the required actions
        print("Move event, {0} was moved to {1}".format(event.src_path,
                                                        event.dest_path))
        self.send_email_alarm("moved", event.src_path)

    def send_email_alarm(self, event_type, event_source_path):
        try:
            email_body = self.config_data.email_bodies_for_events[event_type]
        except Exception as e:
            print("I got a Key Error - reason: item {} is not defined.\n\
                  Please correct before continueing".format(str(e)))
            exit(0)

        # create email subject
        subject = email_body["subject"] + " Alarm Date: " +\
            datetime.now().strftime('%Y/%m/%d')

        # create the email instance
        email_instance = sendemail.SendEmail(self.email_sender,
                                             self.email_receiver_list,
                                             subject,
                                             self.__smtp_password,
                                             self.smtp_server,
                                             self.smtp_port, self.use_tls)

        # Build the email body
        email_instance.html_add("<h1>Pertaining to " + event_source_path +
                                ":</h1>")
        self.build_email_body(email_instance, email_body)

        # And finally send the email
        email_instance.send()

    def build_email_body(self, email_instance, email_body):
        """ Use the pre-configured standard email body defined in the config file

        :param email_instance: SendEmail object, created based on SMTP servr config
        :param email_body: Class Dict from JSONData, containg body content elements
        """
        for item in email_body["body"]:
            if "html" in item.keys():
                email_instance.html_add(item["html"])
            elif "html_block" in item.keys():
                # Support the inclusion of multi-line HTML/text string blocks
                email_instance.html_add("\n".join(item["html_block"]))
            elif "image" in item.keys():
                email_instance.add_inline_image(item["image"])
            elif "attach" in item:
                email_instance.add_attach(item["attach"])


if __name__ == "__main__":
    # Process the various commandline options
    parser = argparse.ArgumentParser(description='Monitor filesystem for events.\
     Send an email alarm on detected an event.')
    parser.add_argument('-c', '--config', type=str,
                        help='The configuration file path and filename.')
    parser.add_argument('-m', '--monitor', type=str,
                        help='The path to be monitored for changes')
    parser.add_argument('-p', '--pattern', type=str,
                        help='The monitor pattern of files/directories')
    parser.add_argument('-a', '--ignore_pattern', type=str,
                        help='The ignore pattern of files and directories')
    parser.add_argument('-i', '--ignore_directories', type=str,
                        help='Must directories be monitored (True/False)')
    parser.add_argument('-s', '--case_sensitive', type=bool,
                        help='Case sensitive monitoring (True/False)')
    parser.add_argument('-r', '--recursive', type=bool,
                        help='Recursive directories Monitoring (True/False)')
    args = parser.parse_args()
    path = args.monitor if args.monitor else '.'
    config_path = args.config if args.config else './config.json'

    patterns = args.pattern if args.pattern else "*"
    ignore_patterns = args.ignore_pattern if args.ignore_pattern else ""
    ignore_directories = args.ignore_directories if args.ignore_directories else True
    case_sensitive = args.case_sensitive if args.case_sensitive else True
    go_recursively = args.recursive if args.recursive else True

    FileChangeMonitor(path, config_path, go_recursively, patterns,
                      ignore_patterns, ignore_directories, case_sensitive).run()
