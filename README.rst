==============================
File System Email Alarm module
==============================

.. image:: https://img.shields.io/badge/Python-3.7-blue
	:alt: Supported Python versions

.. image:: https://img.shields.io/badge/license-MIT-green
	:alt: MIT License

These python scirpts can be used to:

- Send a multipart MIME encoded email to one or more recipients. The script make provision for adding inline images and attachments.
- Monitor a directory for file activity (create, delete, modify).

On an event the default behaviour is to send a preconfigured email to recipients.

=================
Table of contents
=================

- `Installation`_

- `Getting Started`_

- `Installation`_

- `Getting started`_

  #. `An Example: Sending and Email`_

  #. `An Example: Monitoring a directory`_

- `Configuration`_

  #. `Configuring the SMTP server`_

  #. `Configuring the Email body`_

- `Usage: alarm_watchdog.py`_

- `Contributing`_

- `License`_


==============================
Installation
==============================

This part will be cleaned up in future commits.

You can download the files from the master repository using the download option in GitLab.

You can clone the project using git clone or fetch

.. code-block:: shell
    $ git clone https://gitlab.com/ex3t3r/file-activity-email-alarm.git


==============================
Getting Started
==============================

------------------------------
An Example: Sending and Email
------------------------------
Test the operation of sending an email using the ``test_sendemail.py`` script.

To use the test script you need to update the ``testconfig.json`` file to include the correct SMTP server details. 
See `Configuring the SMTP server`_ for details on what is required.

Ensure that you have the two test images in the same directory as the script. 
Alternatively you need to update the ``testconfig.json`` file to include the full paths and filenames of the test images to use. 
The two lines in ``testconfig.json`` to be edited are lines 47 and 57.

.. code-block:: json

	{"image": "./test_image.png"}
	{"attach": ["./test_image2.png"]}

Execute the script from the shell using:

.. code-block:: shell

	$ python ./test_sendemail.py ../config/testconfig.json


------------------------------------
An Example: Monitoring a directory
------------------------------------
To monitor a directory for any files being created, deleted, modified or moved, you use the ``alarm_watchdog.py`` script. 
For a more complete usage description see `Usage: alarm_watchdog.py`_

To use the scripts you need to first update the SMTP configuration parameters in the config file, 
see `Configuring the SMTP server`_.

Build the email bodies that should be used for each of the events.  
See `Configuring the Email body`_ for details on how to configure the email body

Execute the script from the shell using:

.. code-block:: shell

	$ python ./alarm_watchdog.py -c ../config/config.json -m ./


To verify that it is working you can create, move, modify and/or delete afile in the directory.
For each action you should see a notice printed on the console and you should recieve an email.


=============
Configuration
=============

----------------------------
Configuring the SMTP server
----------------------------
For the SMTP server to work the following parameters are to be configured in a configuration file.  
The default configuration file that will be used is ``./config.json``

.. code-block:: rst

		smtp_server    : String. The SMTP domain, e.g. "smtp.gmail.com".
		smtp_port      : Integer. The SMTP port.  The default is 587.
		smtp_over_tls  : Boolean. Indicate if TLS must be used. The default is True.
		smtp_password  : String. The account holders password.
		email_sender   : String. The email address of the sender. E.g. "sender_email@gmail.com".
		email_receiver : List of Strings. The email addresses of all the receivers. E.g. ["receiver_email1@domain1.com", "receiver_email2@gdomain2.com"]

Both TLS and SSL is supported.

---------------------------
Configuring the Email body
---------------------------
For ``alarm_watchdog.py`` you can define a specific email template to be used for each event individually.  
The email templates are configured in the *config.json* file.

Each email template can contain one or more elements of:
- html
- html_block
- image
- attach

Each body can contain 1 or more of each of the elements. 
The order in which you define the elements will reflect in the final created email message.

**html**

    This is a single continuous string of text that will be inserted as a paragraph.  
    The text can also include some embedded HTML tags.
    
    *Example*:
    
    .. code-block:: html
    
        "This is some text to display. <BR/> You can even include a list like this <OL><LI>Coffee</LI><LI>Tea</LI></OL> if you want but its not easy to read."

**html_block**

    This is a list of strings containing the necessary content. 
    The list of strings will be internally combined to form a single multiple line string.
    This approach was followed because JSON do not support multiple line strings.
    
    *Example*
    
    .. code-block:: html
    
            ["<h2>HTML Table</h2>",
        	"<table>",
          	"<tr><th>Company</th> <th>Contact</th> <th>Country</th>",
          	"</tr><tr>",
            "<td>Cool Company</td> <td>Braun Werner</td> <td>Germany</td>",
          	"</tr><tr>",
          	"</tr></table>}"
    		]

**image**

    This allows you to include an image inline into the body of the email. 
    You need to provide the full path and filename to the image to include.
    
    *Example*
    
    .. code-block:: html
    
        "/Users/myuser/Documents/images/some_graph.png"
        
**attach**

    This is a list of files to be attached to the email.
    
    *Example*
    
    .. code-block:: html
    
        ["/[path]/file1","/[path]/file2","/[path]/file3"]
        

===========================
Usage: alarm_watchdog.py
===========================
The *alarm_watchdog.py* script has a number of commandline parameters that can be used to change the scripts behaviour.

*Commandline Parameters: (All optional)*
   - **-m, --monitor:** The path to monitor. *Default:* current folder.
   - **-c, --config:** config file Path and filename. *Default:*  ./config.json
   - **-p, --pattern:** Monitoring pattern. *Default:* '*'
   - **-a, --ignore_pattern:** The ignore pattern. *Default:* '' (An empty string)
   - **-i, --ignore_directories:** Directories to be monitored: *Default:* True
   - **-s, --case_sensitive:** Case sensitive monitoring. *Default:* True
   - **-r, --recursive:** Recursive directories Monitoring. *Default:* True


The *pattern*, *ignore_pattern* can be defined as per the documentation and examples at `watchdog <https://pypi.org/project/watchdog/>`_.

=============
Contributing
=============
Contributions are welcome. You can assist with:

- Additional examples and documentation.

- Fixing of issues.

========
License
========

MIT